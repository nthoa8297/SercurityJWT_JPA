package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;

import com.example.config.DataSourceConfig;

@SpringBootApplication
@ComponentScan(excludeFilters={@ComponentScan.Filter(type= FilterType.ASSIGNABLE_TYPE, value=DataSourceConfig.class)})

public class SercurityJwtJpaApplication {

	public static void main(String[] args) {
		SpringApplication.run(SercurityJwtJpaApplication.class, args);
	}

}
