package com.example.config;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import com.example.serviceImp.UserServiceImp;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {

	@Nonnull
	@Resource
	UserServiceImp userDetailsService;

	@Autowired 
	BCryptPasswordEncoder passwordEncoder;
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		final String username = (authentication.getPrincipal() == null) ? "NONE_PROVIDED" : authentication.getName();
		final String password = (String) authentication.getCredentials();
		if (StringUtils.isEmpty(username)) {
			throw new BadCredentialsException("invalid login details");
		}
		// get user details using Spring security user details service
		UserDetails user = null;
		try {
			user = userDetailsService.loadUserByUsername(username);
			if (passwordEncoder.matches(password, user.getPassword())) {
				return new UsernamePasswordAuthenticationToken(user.getUsername(),
						authentication.getCredentials(), user.getAuthorities());
//				return createSuccessfulAuthentication(user, authentication);
			}
		} catch (UsernameNotFoundException exception) {
			throw new BadCredentialsException("invalid login details");
		}
		  throw new BadCredentialsException("invalid login details");
		 
	}

	private Authentication createSuccessfulAuthentication(final UserDetails user, final Authentication authentication) {
		UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(user.getUsername(),
				authentication.getCredentials(), user.getAuthorities());
		token.setDetails(new BCryptPasswordEncoder().encode(user.getUsername() + user.getPassword()));
		return token;
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return (UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication));
	}
}