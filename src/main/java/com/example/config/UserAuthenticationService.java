//package com.example.config;
//
//import java.util.Optional;
//
//import com.example.entity.CustomUserDetails;
//
//public interface UserAuthenticationService {
//
//	Optional<String> login(String username, String password);
//
//	Optional<CustomUserDetails> findByToken(String token);
//
//	void logout(CustomUserDetails user);
//}
