//package com.example.config;
//
//import java.io.IOException;
//
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//import org.springframework.security.core.AuthenticationException;
//import org.springframework.security.web.AuthenticationEntryPoint;
//import org.springframework.stereotype.Component;
//
//@Component
//public class JwtAuthenticationEntryPoint implements AuthenticationEntryPoint {
//	//xử lý các request không có token
//	@Override
//	public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException e)
//			throws IOException, ServletException {
//		System.out.println(String.format("Responding with unautheorized error. Message - %s", e.getMessage()));
//		response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
//	}
//}