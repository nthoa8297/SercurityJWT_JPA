package com.example.config;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.example.entity.FunctionEntity;
import com.example.entity.FunctionRequestEntity;
import com.example.entity.RoleEntity;
import com.example.service.FunctionService;
import com.example.serviceImp.UserServiceImp;

@Configurable
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	@Autowired
	UserServiceImp usersService;

	@Autowired
	FunctionService functionService;

	@Autowired
	private CustomAuthenticationProvider authProvider;

//	TokenAuthenticationProvider provider;
//	WebSecurityConfig(final TokenAuthenticationProvider provider) {
//		super();
//		this.provider = requireNonNull(provider);
//	}

//	private static final RequestMatcher PUBLIC_URLS = new OrRequestMatcher(new AntPathRequestMatcher("/public/**"));
//	private static final RequestMatcher PROTECTED_URLS = new NegatedRequestMatcher(PUBLIC_URLS);

	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
		return bCryptPasswordEncoder;
	}

	@Override // verify user from login
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(authProvider);
	}

//	@Autowired
//	private JwtAuthenticationEntryPoint unauthorizedHandler;

	@Bean(BeanIds.AUTHENTICATION_MANAGER)
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

//	@Bean
//	public Authentication authenticate(Authentication auth) throws AuthenticationException {
//		String username = auth.getName();
//		String password = auth.getCredentials().toString();
//		// to add more logic
//		return new UsernamePasswordAuthenticationToken(username, password, auth.getAuthorities());
//	}

	@Bean
	SimpleUrlAuthenticationSuccessHandler successHandler() {
		final SimpleUrlAuthenticationSuccessHandler successHandler = new SimpleUrlAuthenticationSuccessHandler();
		successHandler.setRedirectStrategy(new NoRedirectStrategy());
		return successHandler;
	}

//	@Bean // Disable Spring boot automatic filter registration.
//	FilterRegistrationBean disableAutoRegistration(final CustomAuthenticationProvider filter) {
//		final FilterRegistrationBean registration = new FilterRegistrationBean(filter);
//		registration.setEnabled(false);
//		return registration;
//	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http.csrf().disable();
		http.formLogin().disable();
		http.httpBasic().disable();
		http.logout().disable();

//		http.exceptionHandling().authenticationEntryPoint(unauthorizedHandler);// xử lý các request không có token
		http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);// session create type
//		http.authenticationProvider(provider)
//				.addFilterBefore(restAuthenticationFilter(), AnonymousAuthenticationFilter.class).authorizeRequests()
//				.requestMatchers(PROTECTED_URLS).authenticated();
//		

		List<FunctionEntity> functionEntities = functionService.getAll();
		Map<String, Collection<FunctionRequestEntity>> map = new HashMap<>();
		map = functionEntities.stream()
				.collect(Collectors.toMap(FunctionEntity::getUrlPath, FunctionEntity::getFunctionRequest));

		for (Entry<String, Collection<FunctionRequestEntity>> entry : map.entrySet()) {
			for (FunctionRequestEntity v : entry.getValue()) {
				HttpMethod method = HttpMethod.GET;
				switch (v.getMethod()) {
				case "POST":
					method = HttpMethod.POST;
					break;
				case "PATH":
					method = HttpMethod.PATCH;
					break;
				case "PUT":
					method = HttpMethod.PUT;
					break;
				default:
					break;
				}
				String url = entry.getKey().equals("/") ? v.getUrlPath() : entry.getKey() + v.getUrlPath();

				List<String> lst = new ArrayList<String>();
				lst = v.getRoles().stream().map(RoleEntity::getName).collect(Collectors.toList());
				StringBuffer role = new StringBuffer();
				role.append(lst.size() > 1 ? "hasAnyAuthority" : "hasAuthority");
				role.append("('");
				role.append(String.join("','", lst));
				role.append("')");
				http.authorizeRequests().antMatchers(method, url).access(role.toString());
			}
		}
		http.authorizeRequests().antMatchers("/public/**").permitAll();
		
		http.addFilterBefore(authenticationFilter(), UsernamePasswordAuthenticationFilter.class);
//	    http.addFilterAt(exceptionTranslationFilter(), ExceptionTranslationFilter.class);
//	    http.addFilter(filterSecurityInterceptor()); // This ensures filter ordering by default
//	    http.addFilterAfter(new CustomFilter(), FilterSecurityInterceptor.class);
	    
//		http.authorizeRequests().anyRequest().authenticated();
//		http.authorizeRequests.antMatcher("/**")
//				.authenticationProvider(custom).authorizeRequests().anyRequest().authenticated();

//		http.addFilterBefore(new JWTLoginFilter("/login", authenticationManager()),
//                UsernamePasswordAuthenticationFilter.class)
		System.out.println("");
//		http.authorizeRequests().and() //
//				.rememberMe().tokenRepository(this.persistentTokenRepository()) //
//				.tokenValiditySeconds(1 * 24 * 60 * 60); // 24h

	}

	public SimpleAuthenticationFilter authenticationFilter() throws Exception {
	    SimpleAuthenticationFilter filter = new SimpleAuthenticationFilter();
	    filter.setAuthenticationManager(authenticationManagerBean());
	    filter.setAuthenticationFailureHandler(new SimpleUrlAuthenticationFailureHandler());
	    return filter;
	}
	
//	@Override
//	public void configure(WebSecurity web) throws Exception {
//		web.ignoring().requestMatchers(PROTECTED_URLS);
//	}

//	@Bean
//	AuthenticationEntryPoint forbiddenEntryPoint() {
//		return new HttpStatusEntryPoint(FORBIDDEN);
//	}

//	@Bean
//	public PersistentTokenRepository persistentTokenRepository() {
//		JdbcTokenRepositoryImpl db = new JdbcTokenRepositoryImpl();
//		db.setDataSource(dataSource);
//		return db;
//	}
}
