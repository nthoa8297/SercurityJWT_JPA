package com.example.config;

import javax.annotation.Nonnull;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.example.serviceImp.UserServiceImp;

public class SimpleAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
	@Nonnull
	@Resource
	UserServiceImp userDetailsService;

	public SimpleAuthenticationFilter(){ 
//		this.setFilterProcessesUrl("/public/user/login");
	}
	
	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException {

		// ...

		UsernamePasswordAuthenticationToken authRequest = getAuthRequest(request);
		setDetails(request, authRequest);

		return this.getAuthenticationManager().authenticate(authRequest);
	}

	private UsernamePasswordAuthenticationToken getAuthRequest(HttpServletRequest request) {

		String username = obtainUsername(request);
		String password = obtainPassword(request);

		// ...

//		UserDetails user = userDetailsService.loadUserByUsername(username);
		return new UsernamePasswordAuthenticationToken(username, password);
	}

//	private Authentication createSuccessfulAuthentication(final UserDetails user, final Authentication authentication) {
//		UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(user.getUsername(),
//				authentication.getCredentials(), user.getAuthorities());
////		token.setDetails(authentication.getDetails());
//		return token;
//	}
}
