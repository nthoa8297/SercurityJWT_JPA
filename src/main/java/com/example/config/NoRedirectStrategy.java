package com.example.config;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.web.RedirectStrategy;

public class NoRedirectStrategy implements RedirectStrategy {
//cấu hình cho restapi khi xảy ra lỗi authen server chỉ cho trả ra 401. chặn toàn bộ redirect
	  @Override
	  public void sendRedirect(final HttpServletRequest request, final HttpServletResponse response, final String url) throws IOException {
	      // No redirect is required with pure REST
	  }

}
