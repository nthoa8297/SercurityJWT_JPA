package com.example.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.entity.CustomUserDetails;
@Repository
public interface CustomUsersDetailRepository extends JpaRepository<CustomUserDetails, Integer> {
	 
	Optional<CustomUserDetails> findById(Integer id);
}
