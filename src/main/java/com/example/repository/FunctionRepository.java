package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.entity.FunctionEntity;
@Repository
public interface FunctionRepository extends JpaRepository<FunctionEntity, Integer>{

}
