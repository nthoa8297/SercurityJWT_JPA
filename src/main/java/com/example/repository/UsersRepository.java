package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.entity.UserEntity;
@Repository
public interface UsersRepository extends JpaRepository<UserEntity, Integer> {
//	@EntityGraph(value = "UserEntity.findByUsernameAndStatusTrueAndGetGroupRolesOfUser", type = EntityGraphType.FETCH)
	UserEntity findByUsernameAndStatusTrue(String userName);
	
	
	 
}
