//package com.example.entity;
//
//import java.io.Serializable;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.Id;
//import javax.persistence.Table;
//
//import lombok.AllArgsConstructor;
//import lombok.Getter;
//import lombok.NoArgsConstructor;
//import lombok.Setter;
//import lombok.ToString;
//
//@Entity
//@Table(name = "user_roles")
//@Getter
//@Setter
//@ToString
//@AllArgsConstructor
//@NoArgsConstructor
//public class UserRolesEntity implements Serializable{
//	@Id
//	@Column(name = "user_id")
//	private Integer userId;
//	@Id
//	@Column(name = "role_id")
//	private Integer roleId;
//}
