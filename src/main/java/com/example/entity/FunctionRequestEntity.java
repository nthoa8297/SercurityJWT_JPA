package com.example.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "function_request")
public class FunctionRequestEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String name;
	
	@Column(name = "url_path")
	private String urlPath;
	
	private boolean status;
	
	@ManyToOne()
	@JoinColumn(name="function_id") 
	@ToString.Exclude
	private FunctionEntity functionId;
	
	private String method;
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(
			name = "role_request", joinColumns = { @JoinColumn(name = "request_id") }, inverseJoinColumns = {
			@JoinColumn(name = "role_id") }
			)
//	@MapKeyJoinColumn(name = "id")
	private List<RoleEntity> roles = new ArrayList<RoleEntity>();

}
