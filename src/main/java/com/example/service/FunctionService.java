package com.example.service;

import java.util.List;

import com.example.entity.FunctionEntity;

public interface FunctionService {
	List<FunctionEntity> getAll();
}
