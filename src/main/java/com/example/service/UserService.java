package com.example.service;

import java.util.List;
import java.util.Optional;

import com.example.entity.CustomUserDetails;
import com.example.entity.UserEntity;

public interface UserService {
	List<UserEntity> getAll();

	UserEntity getByUserName(String userName);

	Optional<CustomUserDetails> findById(String id);
}
