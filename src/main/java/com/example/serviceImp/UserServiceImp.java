package com.example.serviceImp;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.example.entity.CustomUserDetails;
import com.example.entity.UserEntity;
import com.example.repository.CustomUsersDetailRepository;
import com.example.repository.UsersRepository;
import com.example.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service("UserServiceImp")
public class UserServiceImp implements UserService, UserDetailsService {

	@Autowired
	EntityManager entityManager;

	private UsersRepository usersRepository;
	private CustomUsersDetailRepository customUsersDetailRepository;

	@Autowired
	public UserServiceImp(UsersRepository usersRepository, CustomUsersDetailRepository customUsersDetailRepository) {
		this.usersRepository = usersRepository;
		this.customUsersDetailRepository = customUsersDetailRepository;
	}

	@Autowired
	ObjectMapper op;

	@Override
	public List<UserEntity> getAll() {
		// TODO Auto-generated method stub
		List<UserEntity> lst = new ArrayList<UserEntity>();
		try {
			lst = usersRepository.findAll();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lst;
	}

	@Override
	@Transactional
	public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
		UserDetails e = findByUsernameAndStatusTrueAndGetGroupRolesOfUser(userName);
		if (e == null) {
			System.out.println("User not found! " + userName);
			throw new UsernameNotFoundException("User " + userName + " was not found in the database");
		}
		System.out.println("Found User: " + e);
		return e;
	}

	@Override
	public UserEntity getByUserName(String userName) {
		UserEntity e = usersRepository.findByUsernameAndStatusTrue(userName);
		if (e == null) {
			System.out.println("User not found! " + userName);
			throw new UsernameNotFoundException("User " + userName + " was not found in the database");
		}
		findByUsernameAndStatusTrueAndGetGroupRolesOfUser(userName);
		return e;
	}

	private CustomUserDetails findByUsernameAndStatusTrueAndGetGroupRolesOfUser(String userName) {
		Query query = entityManager.createQuery("select u from " + CustomUserDetails.class.getName() + " u "
				+ "where u.status = true  and u.username = :userName").setParameter("userName", userName);
		CustomUserDetails customUserDetails = (CustomUserDetails) query.getSingleResult();
		return customUserDetails;
	}

	@Override
	public Optional<CustomUserDetails> findById(String id) {
		// TODO Auto-generated method stub
		System.out.println(id);
		return customUsersDetailRepository.findById(1);
	}
}
