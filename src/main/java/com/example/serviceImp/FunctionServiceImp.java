package com.example.serviceImp;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.entity.FunctionEntity;
import com.example.repository.FunctionRepository;
import com.example.service.FunctionService;
@Service
public class FunctionServiceImp implements FunctionService{

	@Autowired
	EntityManager entityManager;
	
	private FunctionRepository repository;
	
	@Autowired
	public FunctionServiceImp(FunctionRepository repository) {
		this.repository = repository;
	}
	
	
	@Override
	public List<FunctionEntity> getAll() {
		// TODO Auto-generated method stub
//		String sql = "select f from "+FunctionEntity.class.getName()+ " f join f.functionRequest";
//		Query query = entityManager.createQuery(sql);
//		List<FunctionEntity> lst = query.getResultList();
		List<FunctionEntity> lst = repository.findAll();
		return lst;
	}

}
