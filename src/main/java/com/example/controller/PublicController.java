package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.dto.LoginRequest;
import com.example.entity.CustomUserDetails;
import com.example.serviceImp.UserServiceImp;

@RestController
@RequestMapping("/public")
public class PublicController {

	@Autowired
	private UserServiceImp userServiceImp;
	
	@Autowired
	private AuthenticationManager authenticationManager;
	
	// Login admin
	@PostMapping(value="/user/login", consumes = MediaType.APPLICATION_JSON_VALUE, 
	        produces = MediaType.APPLICATION_JSON_VALUE)
		public ResponseEntity<?> authenticateAdmin(@AuthenticationPrincipal CustomUserDetails userDetails, @RequestBody LoginRequest loginRequest){
			
			Authentication authentication = authenticationManager.authenticate(
					new UsernamePasswordAuthenticationToken(loginRequest.getUsername(),loginRequest.getPassword()));
	       //Set chuỗi authentication đó cho UserPrincipal
			SecurityContextHolder.getContext().setAuthentication(authentication);
			Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			 String username = "null";
			if (principal instanceof UserDetails) {
			   username = ((UserDetails)principal).getUsername();
			} else {
			  username = principal.toString();
			}
//			CustomUserDetails accountLogin = userServiceImp.loadUserByUsername(loginRequest);
//			Set<Role> roles = accountLogin.getRole();
//			boolean isAdmin = false;
//			for (Role role : roles) {
//				if (role.getName().equals("ADMIN")) {
//					isAdmin = true;
//				}
//			}
//			if (authentication != null && isAdmin) {
//				String jwt = tokenProvider.generateToken(authentication);
				return ResponseEntity.ok(userDetails); //Trả về chuỗi jwt(authentication string)
//			}
		}
	
	@GetMapping("/user/logout")
	public ResponseEntity<?> logout(){
		SecurityContextHolder.getContext().setAuthentication(null);
		return ResponseEntity.ok("logout");
	}

}
